import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighLight]'
})
export class HighLightDirective implements OnInit {

  constructor(private elementRef:ElementRef) {

    this.elementRef.nativeElement.style.backgroundColor = 'green';
    console.log("print Directive", this.elementRef.nativeElement.style.backgoundColor);
  }

  ngOnInit(){

  }

}
