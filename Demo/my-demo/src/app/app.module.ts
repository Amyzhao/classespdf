import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { FormsModule} from "@angular/forms";
import { HighLightDirective } from './high-light.directive';
import { BetterHighLightDirective } from './betterHighLight/better-high-light.directive';
import { HeroService } from './hero.service'

@NgModule({
  declarations: [
    AppComponent,
    ShoppingListComponent,
    HighLightDirective,
    BetterHighLightDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [HeroService],
  bootstrap: [AppComponent]
})
export class AppModule { }
