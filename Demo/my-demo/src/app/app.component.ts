import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title:string = 'Shopping Page';
  disabledButton:boolean = false;
  clickTimes:number = 0;
  name:string = 'value';
  value:string = '';

  showNgIf:boolean =  false;
  //ngfor
  cars = ['car1','car2','car3'];

  @ViewChild('inputValue') inputContent: ElementRef;

  num:number = 13;
  clicked(){
    this.clickTimes++;
  }

  input(event:any){
    console.log("log",event);
    this.value = (<HTMLInputElement>event.target).value;
  }

  showAccount(event:any){
    console.log("print",event);
  }

  showInputValue(){
    console.log("print Value", this.inputContent.nativeElement.value);
  }








}
