import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss'],
  encapsulation: ViewEncapsulation.Native
  
})
export class ShoppingListComponent implements OnInit {

  @Input('shoppingAmount') amount:number;
  @Output() shoppingClicked = new EventEmitter();


  constructor() { }

  click(){
    this.shoppingClicked.emit("20");
  }

  ngOnInit() {

  }

}
